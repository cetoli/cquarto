"""
############################################################
Quarto - Visual
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: 2013/06/16
:Status: This is a "work in progress"
:Revision: 0.1.1
:Home: `Labase <http://labase.selfip.org/>`__
:Copyright: 2013, `GPL <http://is.gd/3Udt>`__.
"""
RAIO = 45
ESP = 10
PASSO = 2 * RAIO + ESP
LP = 2 * PASSO + ESP
LG = 4 * PASSO + ESP
BASE = LP + LG + 3 * ESP

class Visual:
    """Monta as pecas do jogo.
    """
    def __init__(self, doc, svg):
        """Constroi as partes do Jogo. """
        meulugar = doc["main"]
        self.svg = svg
        self.canvas = svg.svg(width = BASE,
        height = BASE)
        meulugar <= self.canvas
        self.base = svg.rect (x = 0, y = 0,
        rx = RAIO, ry = RAIO, width = BASE, height = BASE,
        fill = "navajowhite")
        self.canvas <= self.base
        self.parte1 = self.svg.rect (x = ESP, y = ESP,
        rx = RAIO, ry = RAIO, width = LG, height = LP,
        fill = "peru")
        self.canvas <= self.parte1
    def monta_parte(self, lugar, x, y, ncw, nch, esp = ESP, cor = 'peru'):
        parte = self.svg.rect (x = x, y = y,
        rx = RAIO, ry = RAIO, width = ncw * PASSO + ESP,
        height = nch * PASSO + ESP,  fill = cor)
        lugar <= parte
        container = self.svg.g(transform = "translate(%d, %d)"%(x, y))
        lugar <= container
        casas = [self.monta_casa(container, x = ESP+ RAIO + (c % ncw) * PASSO,
            y = ESP + RAIO + (c // ncw) * PASSO) for c in range(ncw*nch)]
        return casas
    def monta_casa(self, lugar, x, y, cor = 'burlywood'):
        casa = self.svg.ellipse(cx = x, cy = y,
            rx = RAIO, ry = RAIO, fill = cor)
        lugar <= casa
        return casa

    def monta_base(self):
        """Monta a casa que fica na base. """
        return self.monta_parte(self.canvas, LG + 2 * ESP, ESP, 1, 1 )


    def monta_tabuleiro(self):
        """Monta o tabuleiro onde se joga as pecas. """
        return self.monta_parte(self.canvas, ESP, LP + 2 * ESP, 4, 4 )

    def monta_mao(self):
        """Monta o espaco onde ficam as pecas no inicio. """
        topo = self.monta_parte(self.canvas, ESP, ESP, 4, 2 )
        lado = self.monta_parte(self.canvas, LG + 2 * ESP, LP + 2 * ESP, 2, 4 )
	return topo + lado

